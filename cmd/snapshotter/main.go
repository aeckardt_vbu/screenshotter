package main

import (
	"context"
	"flag"
	"golang.org/x/sync/semaphore"
	"log"
	"net/url"
	"os"
	"path"
	"snapshotter/pkg/screenshot"
	"snapshotter/pkg/traversal"
	"strings"
	"sync"
)

var _url string
var outputDir string
var concurrency int64

func printUsage() {
	println("Usage: snapshotter -url <url> -output-dir <dir> [-concurrency <int>]")
	println("Flags:")
	println("  -url <url>            url to take screenshots of")
	println("  -output-dir <dir>     directory to save screenshots to")
	println("  -concurrency <int>    number of concurrent requests to make. Default: 6")
}

func main() {
	// parse flags
	flag.StringVar(&_url, "url", "", "url to take screenshots of")
	flag.StringVar(&outputDir, "output-dir", "screenshots", "directory to save screenshots to")
	flag.Int64Var(&concurrency, "concurrency", 6, "number of concurrent requests to make")

	flag.Parse()

	if _url == "" {
		printUsage()
		os.Exit(1)
	}

	initial, err := url.Parse(_url)
	if err != nil {
		println("invalid url")
		os.Exit(1)
	}

	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		if err := os.Mkdir(outputDir, 0755); err != nil {
			panic(err)
		}
	}

	traverser := traversal.NewPageTraverser(initial)

	urls := traverser.Traverse()

	sem := semaphore.NewWeighted(concurrency)
	ctx := context.TODO()
	var wg sync.WaitGroup

	sb := strings.Builder{}
	for _, u := range urls {
		u := u
		sb.WriteString(u.String() + "\n")

		wg.Add(1)

		go func() {
			sem.Acquire(ctx, 1)
			defer sem.Release(1)
			defer wg.Done()

			buf, err := screenshot.CaptureScreenshot(u.String())
			if err != nil {
				panic(err)
			}

			// replace / with _ in url
			replacedURL := strings.NewReplacer("/", "_").Replace(u.EscapedPath())
			replacedURL = strings.Trim(replacedURL, "_")

			if err := os.WriteFile(path.Join(outputDir, replacedURL+".jpg"), buf, 0644); err != nil {
				panic(err)
			}
		}()
	}

	// save urls to csv file
	if err := os.WriteFile(path.Join(outputDir, "urls.csv"), []byte(sb.String()), 0644); err != nil {
		log.Fatal(err)
	}

	wg.Wait()
}
