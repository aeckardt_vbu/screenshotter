# Snapshotter

## Description

This is a simple tool that traverses the links on a website and takes a screenshot of each page. It only traverses links that are on the same domain as the starting URL.

## Pre-requisites

- [Go](https://golang.org/doc/install) 1.13 or higher
- [Chrome](https://www.google.com/chrome/) or [Chromium](https://www.chromium.org/getting-involved/download-chromium) installed
- [Make](https://www.gnu.org/software/make/) (optional)

## Building

Run `make` or `go build -o snapshotter cmd/snapshotter/main.go`. This will create a binary called `snapshotter` in the `build` directory.

## Usage

```
Usage: snapshotter -url <url> -output-dir <dir> [-concurrency <int>]
Flags:
  -url <url>            url to take screenshots of
  -output-dir <dir>     directory to save screenshots to
  -concurrency <int>    number of concurrent requests to make. Default: 6
```

## Example

```
snapshotter -url https://meine-krankenkasse.de -output-dir ./screenshots
```

## Testing

Run `make test` or `go test ./...` to run the tests.
