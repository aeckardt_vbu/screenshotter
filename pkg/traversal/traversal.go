package traversal

import (
	"net/url"
)

// A PageTraverser is a type that can traverse a website's pages.
type PageTraverser interface {
	Traverse() []*url.URL
}

type PageTraverserImpl struct {
	initial *url.URL
}

func NewPageTraverser(initial *url.URL) PageTraverser {
	return &PageTraverserImpl{
		initial: initial,
	}
}

func (p *PageTraverserImpl) Traverse() []*url.URL {
	safeList := SafeUrlList{urls: make(map[string]bool)}
	wg.Add(1)
	go scrape(p.initial.String(), p.initial.Host, &safeList)
	wg.Wait()

	return safeList.getURLs()
}
