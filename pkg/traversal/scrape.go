package traversal

import (
	"golang.org/x/net/html"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
)

var wg sync.WaitGroup
var urlRegex = regexp.MustCompile(`^(https?://|/).*`)

type SafeUrlList struct {
	urls map[string]bool
	mux  sync.Mutex
}

func (s *SafeUrlList) Exists(key string) bool {
	s.mux.Lock()
	defer s.mux.Unlock()
	_, ok := s.urls[key]
	return ok
}

func (s *SafeUrlList) Add(key string) {
	s.mux.Lock()
	s.urls[key] = true
	s.mux.Unlock()
}

func (s *SafeUrlList) getURLs() []*url.URL {
	s.mux.Lock()
	defer s.mux.Unlock()
	urls := make([]*url.URL, 0, len(s.urls))
	for k := range s.urls {
		u, err := url.Parse(k)
		if err != nil {
			continue
		}

		urls = append(urls, u)
	}

	// filter unique urls
	uniqueUrls := make([]*url.URL, 0, len(urls))
	uniqueUrlsMap := make(map[string]bool)
	for _, u := range urls {
		if !uniqueUrlsMap[u.String()] {
			uniqueUrlsMap[u.String()] = true
			uniqueUrls = append(uniqueUrls, u)
		}
	}

	return uniqueUrls
}

func scrape(u string, baseDomain string, safeList *SafeUrlList) {
	defer wg.Done()

	// remove hash
	u = strings.Split(u, "#")[0]

	if safeList.Exists(u) {
		return
	}
	safeList.Add(u)

	c := http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			if len(via) >= 10 {
				return http.ErrUseLastResponse
			}
			if req.URL.Host != baseDomain {
				return http.ErrUseLastResponse
			}

			return nil
		},
	}

	resp, err := c.Get(u)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	b := resp.Body
	z := html.NewTokenizer(b)

	for {
		tt := z.Next()
		switch {
		case tt == html.ErrorToken:
			return
		case tt == html.StartTagToken:
			t := z.Token()

			isAnchor := t.Data == "a"
			if isAnchor {
				for _, a := range t.Attr {
					if a.Key == "href" {
						if urlRegex.MatchString(a.Val) == false {
							continue
						}

						absolute := getAbsoluteURL(a.Val, u)
						if absolute != "" {
							parsedUrl, err := url.Parse(absolute)
							if err != nil {
								continue
							}
							if parsedUrl.Host == baseDomain {
								wg.Add(1)
								go scrape(absolute, baseDomain, safeList)
							}
						}
					}
				}
			}
		}
	}
}

func getAbsoluteURL(href, base string) string {
	uri, err := url.Parse(href)
	if err != nil {
		return ""
	}
	baseURL, err := url.Parse(base)
	if err != nil {
		return ""
	}
	uri = baseURL.ResolveReference(uri)
	return uri.String()
}
