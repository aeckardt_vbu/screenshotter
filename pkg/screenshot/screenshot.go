package screenshot

import (
	"context"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
)

// CaptureScreenshot takes a screenshot of a webpage. It returns the screenshot as a byte slice of a jpeg.
func CaptureScreenshot(url string) ([]byte, error) {
	// create new browser context
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	// navigate to the webpage, wait for it to load
	if err := chromedp.Run(ctx, chromedp.Navigate(url)); err != nil {
		return nil, err
	}

	// capture screenshot
	var buf []byte
	if err := chromedp.Run(ctx, FullScreenshot(90, &buf)); err != nil {
		return nil, err
	}

	return buf, nil
}

func FullScreenshot(quality int64, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.ActionFunc(func(ctx context.Context) error {
			// get layout metrics
			_, _, _, _, _, cssContentSize, err := page.GetLayoutMetrics().Do(ctx)
			if err != nil {
				panic(err)
			}

			width, height := int64(cssContentSize.Width), int64(cssContentSize.Height)

			// capture screenshot
			*res, err = page.CaptureScreenshot().
				WithQuality(quality).
				WithCaptureBeyondViewport(true).
				WithClip(&page.Viewport{
					X:      cssContentSize.X,
					Y:      cssContentSize.Y,
					Width:  float64(width),
					Height: float64(height),
					Scale:  1,
				}).Do(ctx)
			if err != nil {
				panic(err)
			}
			return nil
		}),
	}
}
